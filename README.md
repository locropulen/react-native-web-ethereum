# react-native-web-ethereum

A cross platform app in `react-native` and in `web`

## Dependencies

* [react-broadcast](https://github.com/ReactTraining/react-broadcast) to handle eficiently the styles in the component tree
* [redux](https://github.com/reactjs/redux) and [redux-form](https://redux-form.com/7.2.0/) to handle the state
* [truffle](https://github.com/trufflesuite/truffle) aiming to make life as an Ethereum developer easier


## Usage

```console
yarn
yarn compile-contracts
yarn web // open localhost:3000
```


## Build Contracts

```console
npm run contracts:build
npm run contracts:deploy
```


## Contribute

PR, stars ✭ and issue reporting, welcome!

## License

MIT