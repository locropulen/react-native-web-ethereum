// @flow

import React, { Component } from "react";

import { Field } from "redux-form"; // 7.2.0
import { Ionicons } from "@expo/vector-icons";
import {
	ScrollView,
	View,
	KeyboardAvoidingView,
	StyleSheet,
} from "react-native";

import { normalize, withTheme, UIField } from "redux-form-ui";

// NativeComponents
import NativeInput from "../NativeComponents/NativeInput";
import NativeSwitch from "../NativeComponents/NativeSwitch";
import NativeTouchable from "../NativeComponents/NativeTouchable";

import type { ThemeType } from "../types";
import type { ITheme } from "redux-form-ui";

// Native Fields
const NameTextField = UIField(NativeInput);
const PhoneTextField = UIField(NativeInput);
const SwitchField = UIField(NativeSwitch);

class MyForm extends Component<{
	theme: ITheme<ThemeType>,
	handleSubmit: (t: Function) => Function,
	onSubmit: Function,
}> {
	componentDidMount() {
		this.ref // the Field
			.getRenderedComponent()
			.getRenderedComponent()
			.focus();
	}
	ref: any;
	saveRef = (ref: any) => (this.ref = ref);
	render() {
		const { theme, handleSubmit, onSubmit } = this.props;

		return (
			<KeyboardAvoidingView behavior="padding" style={styles.main}>
				<ScrollView style={theme.Form}>
					<Field
						name="switch"
						component={SwitchField}
						floatingLabelText="Switch me"
					/>
					<Field
						name="name"
						component={NameTextField}
						hintText="Name"
						ref={this.saveRef}
						withRef
						floatingLabelText="Name"
						placeholder="Name"
					/>
					<Field
						name="phone"
						component={PhoneTextField}
						normalize={normalize.toPhone}
						keyboardType="phone-pad"
						hintText="Phone"
						floatingLabelText="Phone"
						placeholder="Phone"
					/>
				</ScrollView>
				<View style={theme.Bottom}>
					<NativeTouchable onPress={handleSubmit(onSubmit)}>
						<Ionicons name="md-add-circle" {...theme.Touchable.Icon} />
					</NativeTouchable>
				</View>
			</KeyboardAvoidingView>
		);
	}
}
export default withTheme(MyForm);

const styles = StyleSheet.create({
	main: {
		flex: 1,
		justifyContent: "space-between",
	},
});
