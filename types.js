/* @flow */
// https://github.com/facebook/react-native/commit/9c29ee1504a66979d7e87f60e42d437e044a1ada

import type { Style } from "redux-form-ui";

export type ThemeType = {
	Container: Style,
	Form: Style,
	Bottom: Style,
	Header: Style,
	TextInput: Style,
	Label: {
		Text: Style,
		View: Style,
	},
	ErrorText: Style,
	Switch: Style,
	Touchable: {
		Text: Style,
		Container: Style,
		Icon: {
			size: number,
			color: string,
			style: Style,
		},
	},
};
